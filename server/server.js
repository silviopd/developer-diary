const express = require("express")
const cors = require("cors") // Importa el middleware cors
const handler = require("./src/files")

const app = express()

const port = process.env.PORT || 3001

// Agrega el middleware cors
app.use(cors())

// Habilita el análisis del cuerpo de la solicitud JSON
app.use(express.json())

app.use(express.static("public"))

app.get("/api/data", (req, res) => {
	res.json({ message: "Hello from Express!" })
})

// Usa el controlador para manejar las solicitudes en /api/files
app.use("/api/files", (req, res) => {
	handler(req, res)
})

app.listen(port, () => {
	console.log(`Server is running on port ${port}`)
})
