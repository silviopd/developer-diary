const fs = require("fs")

function handler(req, res) {
	const datosBody = req.body
	const datosHeader = req.headers
	let id = ""

	switch (req.method) {
		//obtener datos
		case "GET":
			fs.readFile(
				"./public/data/" + datosHeader.filename,
				"utf8",
				(err, data) => {
					if (err) {
						console.error("Error al leer el archivo JSON:", err)
						res.status(500).json({ error: "Error interno del servidor" })
						return
					}
					const folders = JSON.parse(data)
					res.status(200).json(folders)
				}
			)
			break
		//agregar datos
		case "POST":
			fs.readFile(
				"./public/data/" + datosHeader.filename,
				"utf8",
				(err, data) => {
					if (err) {
						console.error("Error al leer el archivo JSON:", err)
						res.status(500).json({ error: "Error interno del servidor" })
						return
					}

					let jsonData = JSON.parse(data)
					jsonData.push(datosBody)

					let newDataJson = JSON.stringify(jsonData, null, 2)

					fs.writeFile(
						"./public/data/" + datosHeader.filename,
						newDataJson,
						"utf8",
						(err) => {
							if (err) {
								console.error("Error al escribir en el archivo JSON:", err)
								res.status(500).json({ error: "Error interno del servidor" })
								return
							}

							console.log("Datos agregados correctamente.")
							res.status(200).json({
								message: "Datos agregados correctamente",
								data: JSON.parse(newDataJson),
							})
						}
					)
				}
			)
			break
		//modificar datos
		case "PUT":
			id = datosHeader.id

			fs.readFile(
				"./public/data/" + datosHeader.filename,
				"utf8",
				(err, data) => {
					if (err) {
						console.error("Error al leer el archivo JSON:", err)
						res.status(500).json({ error: "Error interno del servidor" })
						return
					}

					let snippets = JSON.parse(data)

					const snippetIndex = snippets.findIndex(
						(snippet) => snippet.id === id
					)
					if (snippetIndex === -1) {
						res.status(404).json({ error: "Snippet no encontrado" })
						return
					}

					snippets[snippetIndex] = { ...snippets[snippetIndex], ...datosBody }

					const newDataJson = JSON.stringify(snippets, null, 2)

					fs.writeFile(
						"./public/data/" + datosHeader.filename,
						newDataJson,
						"utf8",
						(err) => {
							if (err) {
								console.error("Error al escribir en el archivo JSON:", err)
								res.status(500).json({ error: "Error interno del servidor" })
								return
							}

							console.log("Datos actualizados correctamente.")
							res.status(200).json({
								message: "Datos actualizados correctamente",
								data: JSON.parse(newDataJson),
							})
						}
					)
				}
			)
			break
		// eliminar datos (aun no esta lista)
		case "DELETE":
			id = datosHeader.id

			fs.readFile(
				"./public/data/" + datosHeader.filename,
				"utf8",
				(err, data) => {
					if (err) {
						console.error("Error al leer el archivo JSON:", err)
						res.status(500).json({ error: "Error interno del servidor" })
						return
					}

					let snippets = JSON.parse(data)

					const snippetIndex = snippets.findIndex(
						(snippet) => snippet.id === id
					)
					if (snippetIndex === -1) {
						res.status(404).json({ error: "Snippet no encontrado" })
						return
					}

					snippets.splice(snippetIndex, 1)

					const newDataJson = JSON.stringify(snippets, null, 2)

					fs.writeFile(
						"./public/data/" + datosHeader.filename,
						newDataJson,
						"utf8",
						(err) => {
							if (err) {
								console.error("Error al escribir en el archivo JSON:", err)
								res.status(500).json({ error: "Error interno del servidor" })
								return
							}

							console.log("Snippet eliminado correctamente.")
							res.status(200).json({
								message: "Snippet eliminado correctamente",
								data: JSON.parse(newDataJson),
							})
						}
					)
				}
			)
			break

		default:
			// Handle unsupported methods
			res.status(405).json({ message: "Method Not Allowed" })
			break
	}
}

module.exports = handler
