import axios from "axios"

const templateFolder = {
	id: "",
	name: "",
	isOpen: false,
	defaultLanguage: "plaintext",
	parentId: null,
	index: 0,
	icon: "fa fa-folder",
	type: "",
	modify: true,
	isSelected: false,
}

const templateStatus = {
	id: "",
	name: "",
}

const templateTags = {
	id: "",
	name: "",
}

const templateTypes = {
	id: "",
	name: "",
}

const templateWorks = {
	id: "",
	name: "",
}

const templateSnippets = {
	id: "",
	name: "",
	folderId: "",
	isFavorites: false,
	content: [],
	tagsIds: [],
	description: "",
	canDelete: true,
	isSelected: false,
}

const templateSnippetsContent = {
	label: "",
	value: "",
	language: "",
	canWrite: true,
}

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL || "http://localhost:3001"

const toastConfig = {
	time: 1500,
	position: "top-right",
}

const monacoConfig = {
	time: 1000,
}

async function getDataJSON(fileName) {
	try {
		const response = await axios.get(`${baseUrl}/api/files`, {
			headers: {
				"Content-Type": "application/json",
				fileName: fileName,
			},
		})
		console.log(fileName, response.data)
		return response.data // Retorna los datos del JSON
	} catch (error) {
		console.error(error)
		throw error // Propaga el error para manejarlo en otro lugar si es necesario
	}
}

async function setDataJSON(fileName, data) {
	try {
		const response = await axios.post(`${baseUrl}/api/files`, data, {
			headers: {
				"Content-Type": "application/json",
				fileName: fileName,
			},
		})
		console.log("setDataJSON", fileName, data, response)
		return response // Retorna los datos del JSON
	} catch (error) {
		console.error(error)
		throw error // Propaga el error para manejarlo en otro lugar si es necesario
	}
}

async function updateDataJSON(fileName, id, data) {
	try {
		const response = await axios.put(`${baseUrl}/api/files`, data, {
			headers: {
				"Content-Type": "application/json",
				fileName: fileName,
				id: id,
			},
		})
		console.log("updateDataJSON", fileName, data, response)
		return response // Retorna los datos del JSON
	} catch (error) {
		console.error(error)
		throw error // Propaga el error para manejarlo en otro lugar si es necesario
	}
}

async function deleteDataJSON(fileName, id) {
	try {
		const response = await axios.delete(`${baseUrl}/api/files`, {
			headers: {
				"Content-Type": "application/json",
				fileName: fileName,
				id: id,
			},
		})
		console.log("deleteDataJSON", fileName, response)
		return response // Retorna los datos del JSON
	} catch (error) {
		console.error(error)
		throw error // Propaga el error para manejarlo en otro lugar si es necesario
	}
}

async function changeStructureJSON(data) {
	const tree = []
	const nodesMap = {}

	// Primero, construimos un mapa de nodos
	data.forEach((item) => {
		nodesMap[item.id] = {
			id: item.id,
			name: item.name,
			children: [],
			className: item.id,
		}
	})

	data.forEach((item) => {
		if (item.parentId && nodesMap[item.parentId]) {
			nodesMap[item.parentId].children.push(nodesMap[item.id])
		} else {
			tree.push(nodesMap[item.id])
		}
	})

	function removeEmptyNodes(node) {
		if (node.children.length === 0) {
			delete node.children
		} else {
			node.children.forEach(removeEmptyNodes)
		}
	}

	tree.forEach(removeEmptyNodes)

	console.log("changeStructureJSON", tree)
	return tree
}

function findParentIds(data, id, parentIds = []) {
	const item = data.find((item) => item.id === id)
	if (!item) return parentIds

	parentIds.unshift(item.id)

	if (item.parentId) {
		return findParentIds(data, item.parentId, parentIds)
	}

	return parentIds
}

function findChildIds(data, id, childIds = []) {
	const item = data.find((item) => item.id === id)
	if (!item) return childIds

	childIds.push(item.id)

	const children = data.filter((child) => child.parentId === id)
	children.forEach((child) => {
		findChildIds(data, child.id, childIds)
	})

	console.log("childIds", childIds)
	return childIds
}

function filterListFolder(data, filter) {
	console.log("filterListFolder", filter, data)
	if (filter.includes("9e60ab5b-c17a-4758-a6fd-955e00d4b2cf")) {
		return data
	} else {
		let desordenado = data.filter((v) => filter.includes(v.folderId))
		let ordenar = []
		filter.forEach((v) => {
			desordenado.forEach((k) => {
				if (v === k.folderId) {
					ordenar.push(k)
				}
			})
		})
		return ordenar
	}
}

function tieneSubfolder(json, id) {
	for (const item of json) {
		if (item.parentId === id) {
			return true
		}
	}
	return false
}

export {
	getDataJSON,
	changeStructureJSON,
	setDataJSON,
	updateDataJSON,
	deleteDataJSON,
	findParentIds,
	findChildIds,
	filterListFolder,
	tieneSubfolder,
	templateFolder,
	templateStatus,
	templateTags,
	templateTypes,
	templateWorks,
	templateSnippets,
	templateSnippetsContent,
	toastConfig,
	monacoConfig,
}
