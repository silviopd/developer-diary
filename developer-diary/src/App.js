import { store } from "./store/store"
import { Provider } from "react-redux"

import Production from "./components/Home/index"

function App() {
	return (
		<Provider store={store}>
			<Production />
		</Provider>
	)
}

export default App
