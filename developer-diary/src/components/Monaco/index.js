import toast from "react-hot-toast"
import { useRef, useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import Editor from "@monaco-editor/react"
import { updateDataJSON, toastConfig, monacoConfig } from "../../helper"

import {
	getAllSnippets,
	fnAllSnippets,
	fnSetSnippetsSelected,
	getSelectedSnippet,
} from "../../store/reducers/snippetsReducer"

import Language from "../LanguageEditor/index"

import { FaEdit, FaTrash, FaLock, FaLockOpen, FaCopy } from "react-icons/fa"

import { Tab, Tabs, TabList, TabPanel } from "react-tabs"
import "react-tabs/style/react-tabs.css"

import copy from "copy-to-clipboard"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

const TIMER_SAVE_ONCHANGE = monacoConfig.time

function App({ data }) {
	console.log("Monaco", data)

	const dispatch = useDispatch()

	const [getSnippetsReduce, setGetSnippetsReduce] = useState([])
	const getAllSnippetsRD = useSelector(getAllSnippets)
	const getSelectedSnippetRD = useSelector(getSelectedSnippet)
	const [inputValueSnippetName, setInputValueSnippetName] = useState("")
	const [modify, setModify] = useState(false)
	const [index, setIndex] = useState(null)
	const timeoutRef = useRef(null)
	const timeoutRef2 = useRef(null)
	const inputRef = useRef(null)

	const [textoTextArea, setTextoTextArea] = useState("")

	useEffect(() => {
		setGetSnippetsReduce(data)
	}, [data, getAllSnippetsRD])

	useEffect(() => {
		if (modify && inputRef.current) {
			inputRef.current.focus()
		}
	}, [modify])

	useEffect(() => {
		setTextoTextArea(getSelectedSnippetRD.description)
	}, [getAllSnippetsRD])

	const handleChangeSnippetName = (event) => {
		setInputValueSnippetName(event.target.value)
	}

	const manejarKeyDown = async (event) => {
		if (event.key === "Enter") {
			let snippetCopy = JSON.parse(JSON.stringify(getSelectedSnippetRD))
			snippetCopy.content[index].label = inputValueSnippetName

			await updateData(snippetCopy)

			setModify(false)
			setIndex(null)
		} else if (event.key === "Escape") {
			setModify(false)
			setIndex(null)
		}
	}

	const updateData = async (data) => {
		let returnAPI = await updateDataJSON("snippets.json", data.id, data)

		dispatch(fnAllSnippets(returnAPI.data.data))
		dispatch(fnSetSnippetsSelected(data))

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	async function handleEditorChange(value, event, index) {
		const updatedContent = [...getSnippetsReduce.content]
		updatedContent[index] = { ...updatedContent[index], value }
		const updatedSnippet = { ...getSnippetsReduce, content: updatedContent }

		if (timeoutRef.current) {
			clearTimeout(timeoutRef.current)
		}

		timeoutRef.current = setTimeout(async () => {
			let returnAPI = await updateDataJSON(
				"snippets.json",
				getSnippetsReduce.id,
				updatedSnippet
			)

			dispatch(fnAllSnippets(returnAPI.data.data))
			dispatch(fnSetSnippetsSelected(updatedSnippet))

			toast.success("Actualizado", {
				duration: TIME_LIMIT,
				position: POSITION_TOAST,
			})
		}, TIMER_SAVE_ONCHANGE)
	}

	const deleteSnippet = async (index) => {
		let snippetCopy = JSON.parse(JSON.stringify(getSnippetsReduce))
		snippetCopy.content.splice(index, 1)

		let returnAPI = await updateDataJSON(
			"snippets.json",
			snippetCopy.id,
			snippetCopy
		)

		dispatch(fnAllSnippets(returnAPI.data.data))
		dispatch(fnSetSnippetsSelected(snippetCopy))

		toast.success("Eliminado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const changeNameSnippet = (index, content) => {
		setModify(true)
		setIndex(index)
	}

	const canWriteMonaco = async (index) => {
		console.log("canWriteMonaco", getSnippetsReduce, index)

		let snippetCopy = JSON.parse(JSON.stringify(getSnippetsReduce))
		snippetCopy.content[index].canWrite = !snippetCopy.content[index].canWrite

		await updateData(snippetCopy)
	}

	const handleClipboard = async (index) => {
		console.log("handleClipboard", getSnippetsReduce, index)
		const clipboard = getSnippetsReduce.content[index].value
		copy(clipboard)

		toast.success("Copiado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const handleTextAreaChange = async (event) => {
		let valueTextArea = event.target.value
		console.log(
			"handleTextAreaChange valueTextArea;",
			valueTextArea,
			textoTextArea
		)
		setTextoTextArea(valueTextArea)

		console.log("valueTextArea;", valueTextArea, textoTextArea)

		if (timeoutRef2.current) {
			clearTimeout(timeoutRef2.current)
		}

		timeoutRef2.current = setTimeout(async () => {
			let snippetCopy = JSON.parse(JSON.stringify(getSelectedSnippetRD))
			snippetCopy.description = valueTextArea

			await updateData(snippetCopy)
		}, TIMER_SAVE_ONCHANGE)
	}

	return (
		<Tabs>
			<TabList>
				{getSnippetsReduce &&
					getSnippetsReduce.content &&
					getSnippetsReduce.content.map((contentItem, index) => (
						<Tab key={index}>
							{modify ? (
								<input
									type="text"
									placeholder=""
									value={inputValueSnippetName}
									onChange={handleChangeSnippetName}
									onKeyDown={manejarKeyDown}
									ref={inputRef}
									style={{ marginBottom: "20px" }}
								/>
							) : (
								<>
									<span style={{ marginRight: "13px" }}>
										{contentItem.label}
									</span>

									<FaEdit
										style={{ cursor: "pointer", marginRight: "5px" }}
										onClick={() => changeNameSnippet(index)}
									/>
									<FaTrash
										style={{ cursor: "pointer" }}
										onClick={() => deleteSnippet(index)}
									/>
								</>
							)}
						</Tab>
					))}
			</TabList>

			{getSnippetsReduce &&
				getSnippetsReduce.content &&
				getSnippetsReduce.content.map((contentItem, index) => (
					<TabPanel key={index}>
						<div style={{ marginTop: "15px" }}>
							<div
								style={{
									display: "flex",
									justifyContent: "space-between",
									marginBottom: "10px",
									padding: "0 10px",
								}}
							>
								<div>
									<label>
										<strong>{getSelectedSnippetRD.name}</strong>
									</label>
									<label> - </label>
									<label style={{ marginRight: "10px" }}>
										<strong>{contentItem.label}</strong>
									</label>
								</div>
								<div>
									<FaCopy
										style={{ cursor: "pointer", marginRight: "10px" }}
										onClick={() => handleClipboard(index)}
									/>
									{contentItem.canWrite ? (
										<FaLock
											style={{ cursor: "pointer", marginRight: "10px" }}
											onClick={() => canWriteMonaco(index)}
										/>
									) : (
										<FaLockOpen
											style={{ cursor: "pointer", marginRight: "10px" }}
											onClick={() => canWriteMonaco(index)}
										/>
									)}
								</div>
							</div>
							<div
								style={{
									display: "flex",
									// justifyContent: "space-between",
									padding: "0 10px",
									marginBottom: "10px",
								}}
							>
								<label style={{ marginRight: "27px" }}>
									<strong>Lenguaje</strong>
								</label>
								<div style={{ display: "flex" }}>
									<Language index={index} />
								</div>
							</div>
							<div
								style={{
									marginBottom: "30px",
									padding: "0 10px",
									display: "flex",
								}}
							>
								<label style={{ marginRight: "10px" }}>
									<strong>Descripción</strong>
								</label>
								<textarea
									value={textoTextArea}
									onChange={(event) => handleTextAreaChange(event)}
									placeholder="Descripción"
									style={{ width: "100%", height: "2em" }}
								></textarea>
							</div>
						</div>
						<hr></hr>
						<Editor
							height="calc(100vh - 240px)"
							language={contentItem.language}
							value={contentItem.value}
							onChange={(value, event) =>
								handleEditorChange(value, event, index)
							}
							options={{
								readOnly: contentItem.canWrite,
								wordWrap: true,
								fontFamily: "'Fira Code'",
								fontLigatures: true,
							}}
						/>
					</TabPanel>
				))}
		</Tabs>
	)
}

export default App
