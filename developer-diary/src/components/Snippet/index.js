import toast from "react-hot-toast"

import { useState, useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"

import { FaFolderPlus } from "react-icons/fa"
import { BsFillFileEarmarkPlusFill } from "react-icons/bs"

import ListFolders from "../ListFolder/index"
import ListFile from "../ListFile/index"
import Monaco from "../Monaco/index"
import Search from "../Search/index"

import { v4 as uuidv4 } from "uuid"

import {
	getAllFolderSnippets,
	getSelectedFolderSnippets,
	fnAllFolderSnippets,
} from "../../store/reducers/folderSnippetsReducer"

import {
	getAllSnippets,
	getSelectedSnippet,
	fnAllSnippets,
	fnSetSnippetsSeach,
	getSearchSnippet,
	getValueSearchSnippet,
} from "../../store/reducers/snippetsReducer"

import {
	findChildIds,
	filterListFolder,
	templateFolder,
	setDataJSON,
	templateSnippets,
	toastConfig,
} from "../../helper"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

export default function Main() {
	const dispatch = useDispatch()

	const [filterListSnippets, setFilterListSnippets] = useState([])

	const getAllFolderSnippetsRD = useSelector(getAllFolderSnippets)

	const getAllSnippetsRD = useSelector(getAllSnippets)

	console.log("getAllSnippetsRD", "getAllSnippetsRD", getAllSnippetsRD)

	const getSelectedFolderSnippetsRD = useSelector(getSelectedFolderSnippets)

	const getSnippetsRD = useSelector(getSelectedSnippet)

	const getSearchSnippetRD = useSelector(getSearchSnippet)
	const getValueSearchSnippetRD = useSelector(getValueSearchSnippet)

	useEffect(() => {
		console.log(
			"setFilterListSnippets",
			getAllSnippetsRD,
			getAllFolderSnippetsRD,
			getSelectedFolderSnippetsRD
		)

		if (getValueSearchSnippetRD != "") {
			setFilterListSnippets(getSearchSnippetRD)
		} else {
			setFilterListSnippets(
				filterListFolder(
					getAllSnippetsRD,
					findChildIds(getAllFolderSnippetsRD, getSelectedFolderSnippetsRD)
				)
			)
		}
	}, [
		getAllSnippetsRD,
		getAllFolderSnippetsRD,
		getSelectedFolderSnippetsRD,
		getSnippetsRD,
		getSearchSnippetRD,
		getValueSearchSnippetRD,
	])

	console.log(
		"filterListSnippets filterListSnippets",
		filterListSnippets,
		getValueSearchSnippetRD,
		getSearchSnippetRD
	)

	const createFolder = async () => {
		console.log("createFolder")

		let id = uuidv4()
		let data = {
			...templateFolder,
			id: id,
			name: "temp-folder",
			type: "foldersSnippets",
		}

		let returnAPI = await setDataJSON("foldersSnippets.json", data)

		dispatch(fnAllFolderSnippets(returnAPI.data.data))

		toast.success("Creado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const createSnippet = async () => {
		console.log("createSnippet")

		let id = uuidv4()

		let data = {
			...templateSnippets,
			id: id,
			name: "temp-file",
			folderId: "a1dc1f81-129b-46bc-975e-beb98265cc16",
			content: [
				{
					label: "Fragment",
					value: "",
					language: "plaintext",
				},
			],
		}

		let returnAPI = await setDataJSON("snippets.json", data)

		dispatch(fnAllSnippets(returnAPI.data.data))

		toast.success("Creado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	return (
		<>
			<div style={{ display: "flex" }}>
				<div
					style={{
						minWidth: "250px",
						padding: "5px 13px",
						overflowY: "auto",
						borderRight: "1px solid black",
					}}
				>
					<div style={{ display: "flex", justifyContent: "end" }}>
						<FaFolderPlus
							onClick={createFolder}
							style={{ marginRight: "10px", cursor: "pointer" }}
						/>
						<BsFillFileEarmarkPlusFill
							onClick={createSnippet}
							style={{ cursor: "pointer" }}
						/>
					</div>

					{getAllFolderSnippetsRD.length > 0 ? (
						<ListFolders data={getAllFolderSnippetsRD} />
					) : null}
				</div>

				<div
					style={{
						minWidth: "300px",
						padding: "5px 13px",
						overflowY: "auto",
						borderRight: "1px solid black",
					}}
				>
					<Search />
					{filterListSnippets.length > 0 ? (
						<ListFile snippetList={filterListSnippets} />
					) : null}
				</div>

				<div style={{ flexGrow: 1, paddingLeft: "10px" }}>
					{filterListSnippets.length > 0 ? (
						<Monaco data={getSnippetsRD} />
					) : null}
				</div>
			</div>
		</>
	)
}
