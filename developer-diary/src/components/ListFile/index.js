import toast from "react-hot-toast"

import { useState, useRef, useEffect } from "react"

import { useDispatch, useSelector } from "react-redux"

import { FaEdit, FaTrash, FaPlus } from "react-icons/fa"
import { MdDragIndicator, MdDeleteForever } from "react-icons/md"

import {
	fnSetSnippetsSelected,
	fnAllSnippets,
	getAllSnippets,
	getSearchSnippet,
	getValueSearchSnippet,
	fnSetSnippetsSeach,
} from "../../store/reducers/snippetsReducer"

import {
	templateSnippetsContent,
	updateDataJSON,
	deleteDataJSON,
	toastConfig,
} from "../../helper"

import { fnSetInitialDrag, fnSetType } from "../../store/reducers/dragReducer"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

const SnippetCard = ({ snippet }) => {
	const dispatch = useDispatch()

	const [modify, setModify] = useState(false)
	const [inputValueFileName, setInputValueFileName] = useState("")

	const inputRef = useRef(null)

	const [isHovered, setIsHovered] = useState(false)

	const getAllSnippetsRD = useSelector(getAllSnippets)

	const searchSnippetsRD = useSelector(getSearchSnippet)
	const valueSearchRD = useSelector(getValueSearchSnippet)

	useEffect(() => {
		if (modify && inputRef.current) {
			inputRef.current.focus()
		}
	}, [modify])

	const selectedCard = async () => {
		console.log("selectedCard", snippet)

		let snippetUnselected = getAllSnippetsRD.filter(
			(v) => v.isSelected === true
		)
		if (snippetUnselected.length > 0) {
			let snippetUnselectedRD = { ...snippetUnselected[0], isSelected: false }
			dispatch(fnSetSnippetsSelected(snippetUnselectedRD))
			await updateData(snippetUnselectedRD)
		}

		let snippetSelected = JSON.parse(JSON.stringify(snippet))
		snippetSelected.isSelected = true
		dispatch(fnSetSnippetsSelected(snippetSelected))
		await updateData(snippetSelected)

		//modificacion el el search
		if (valueSearchRD != "") {
			let ssrd = JSON.parse(JSON.stringify(searchSnippetsRD))
			ssrd.forEach((v) => {
				v.isSelected = false
				if (v.id == snippet.id) {
					v.isSelected = true
				}
			})
			console.log("dispatch(fnSetSnippetsSeach(ssrd))", ssrd, searchSnippetsRD)
			dispatch(fnSetSnippetsSeach(ssrd))
		}

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const createAnotherFragment = async () => {
		let snippetCopy = JSON.parse(JSON.stringify(snippet))

		let data = {
			...templateSnippetsContent,
			label: "Fragment",
			value: "",
			language: "plaintext",
		}

		snippetCopy.content.push(data)

		console.log("snippetCopy", snippetCopy)

		await updateData(snippetCopy)
	}

	const handleChangeFolderName = (event) => {
		setInputValueFileName(event.target.value)
	}

	const changeNameSnippet = () => {
		setModify(true)
	}

	const manejarKeyDown = async (event) => {
		if (event.key === "Enter") {
			let snippetCopy = JSON.parse(JSON.stringify(snippet))
			snippetCopy.name = inputValueFileName

			console.log("snippetCopy", snippetCopy)

			await updateData(snippetCopy)

			setModify(false)
		} else if (event.key === "Escape") {
			setModify(false)
		}
	}

	const deleteSnippet = async () => {
		let snippetCopy = JSON.parse(JSON.stringify(snippet))
		console.log("deleteSnippet", snippetCopy)

		let returnAPI = await deleteDataJSON(
			"snippets.json",
			snippetCopy.id,
			snippetCopy
		)

		dispatch(fnAllSnippets(returnAPI.data.data))
		if (returnAPI.data.data.length > 0) {
			dispatch(fnSetSnippetsSelected(returnAPI.data.data[0]))
		}

		toast.success("Eliminado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const updateData = async (data) => {
		let returnAPI = await updateDataJSON("snippets.json", data.id, data)

		console.log("updateData", data, returnAPI)

		dispatch(fnAllSnippets(returnAPI.data.data))
		dispatch(fnSetSnippetsSelected(data))

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const handleDragStart = (item) => {
		console.log("handleDragStart", item)
		// setDraggedItem(item)
		dispatch(fnSetInitialDrag(item))
		dispatch(fnSetType("file"))
	}

	const canDelete = async () => {
		console.log("canDelete")

		let snippetCopy = JSON.parse(JSON.stringify(snippet))
		snippetCopy.canDelete = !snippetCopy.canDelete

		console.log("snippetCopy", snippetCopy)

		await updateData(snippetCopy)
	}

	return (
		<div
			style={{
				border: "1px solid #ccc",
				padding: "10px",
				marginBottom: "10px",
				background: snippet.isSelected ? "lavender" : null,
				borderRadius: "10px",
			}}
		>
			<div
				style={{ display: "flex", justifyContent: "space-between" }}
				onMouseEnter={() => setIsHovered(true)}
				onMouseLeave={() => setIsHovered(false)}
			>
				<div
					onClick={() => selectedCard()}
					style={{ cursor: "pointer", width: "165px" }}
				>
					{modify ? (
						<input
							type="text"
							placeholder=""
							onChange={handleChangeFolderName}
							onKeyDown={manejarKeyDown}
							ref={inputRef}
						/>
					) : (
						<div
							style={{
								fontWeight: snippet.isSelected ? "bold" : null,
								fontSize: "15px",
								overflow: "hidden",
								textOverflow: "ellipsis",
								whiteSpace: "nowrap",
								maxWidth: "165px",
							}}
						>
							{snippet.name}
						</div>
					)}
					{/* <p>
					<strong>Tags:</strong> {snippet.tagsIds.join(", ")}
				</p> */}

					{/* {snippet.hasOwnProperty("workId") ? (
					<p>
						<strong>Work ID:</strong> {snippet.workId}
					</p>
				) : null} */}

					{/* {snippet.hasOwnProperty("typeId") ? (
					<p>
						<strong>Type ID:</strong> {snippet.typeId}
					</p>
				) : null} */}

					{/* {snippet.hasOwnProperty("statusId") ? (
					<p>
						<strong>Status ID:</strong> {snippet.statusId}
					</p>
				) : null} */}
				</div>

				<div>
					{isHovered ? (
						<>
							<FaPlus
								onClick={createAnotherFragment}
								style={{ marginRight: "10px", cursor: "pointer" }}
							/>
							<FaEdit
								onClick={changeNameSnippet}
								style={{ marginRight: "10px", cursor: "pointer" }}
							/>
							{snippet.canDelete ? (
								<FaTrash
									onClick={deleteSnippet}
									style={{ cursor: "pointer" }}
								/>
							) : null}
						</>
					) : null}
				</div>

				<div style={{ display: "flex", cursor: "pointer" }}>
					<div>
						<MdDeleteForever onClick={() => canDelete()} />
					</div>
					<div onDragStart={() => handleDragStart(snippet)} draggable>
						<MdDragIndicator />
					</div>
				</div>
			</div>
		</div>
	)
}

const SnippetList = ({ snippetList }) => {
	return (
		<div>
			{snippetList.map((snippet) => (
				<SnippetCard key={snippet.id} snippet={snippet} />
			))}
		</div>
	)
}

export default SnippetList
