import toast from "react-hot-toast"

import React, { useState, useEffect } from "react"
import * as monaco from "monaco-editor"

import {
	getSelectedSnippet,
	fnSetSnippetsSelected,
	fnAllSnippets,
} from "../../store/reducers/snippetsReducer"
import { updateDataJSON, toastConfig } from "../../helper"
import { useDispatch, useSelector } from "react-redux"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

const LanguageSelector = ({ index }) => {
	const dispatch = useDispatch()

	const [languages, setLanguages] = useState([])
	const [selectedLanguage, setSelectedLanguage] = useState("")

	const getSelectedSnippetRD = useSelector(getSelectedSnippet)

	useEffect(() => {
		const fetchLanguages = async () => {
			const languages = monaco.languages.getLanguages()
			console.log("languages", languages)
			setLanguages(languages)
		}
		console.log("getSelectedSnippetRD", getSelectedSnippetRD)
		if (
			languages.length > 0 &&
			getSelectedSnippetRD &&
			getSelectedSnippetRD?.content.length > 0
		) {
			setSelectedLanguage(getSelectedSnippetRD.content[index]?.language)
		} else {
			fetchLanguages()
		}
	}, [index, getSelectedSnippetRD, languages])

	const handleLanguageChange = async (event) => {
		setSelectedLanguage(event.target.value)

		let snippetCopy = JSON.parse(JSON.stringify(getSelectedSnippetRD))
		snippetCopy.content[index] = {
			...snippetCopy.content[index],
			language: event.target.value,
		}

		await updateData(snippetCopy)
	}

	const updateData = async (data) => {
		let returnAPI = await updateDataJSON("snippets.json", data.id, data)

		dispatch(fnAllSnippets(returnAPI.data.data))
		dispatch(fnSetSnippetsSelected(data))

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	return (
		<div>
			<select
				id="languageSelect"
				value={selectedLanguage}
				onChange={handleLanguageChange}
			>
				{languages.map((language) => (
					<option key={language.id} value={language.id}>
						{language.id}
					</option>
				))}
			</select>
		</div>
	)
}

export default LanguageSelector
