import toast from "react-hot-toast"

import { useState, useEffect, useRef } from "react"

import { FaFolder, FaEdit, FaTrash } from "react-icons/fa"
import { MdDragIndicator, MdOutlineKeyboardArrowDown } from "react-icons/md"

import { useDispatch, useSelector } from "react-redux"

import {
	fnSetFolderSnippetsSelected,
	fnAllFolderSnippets,
	getAllFolderSnippets,
} from "../../store/reducers/folderSnippetsReducer"

import {
	fnSetInitialDrag,
	fnSetFinalDrag,
	fnSetType,
	getInitialDrag,
	getFinalDrag,
	getTypeDrag,
} from "../../store/reducers/dragReducer"

import {
	updateDataJSON,
	deleteDataJSON,
	findChildIds,
	tieneSubfolder,
	toastConfig,
} from "../../helper"

import {
	fnAllSnippets,
	getAllSnippets,
	fnSetSnippetsSelected,
} from "../../store/reducers/snippetsReducer"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

const TreeView = ({ data }) => {
	const dispatch = useDispatch()

	const getAllFolderSnippetsRD = useSelector(getAllFolderSnippets)

	const [expandedItems, setExpandedItems] = useState([])
	const [hoveredItem, setHoveredItem] = useState(null)

	// const [draggedItem, setDraggedItem] = useState(null)
	// const [dropTarget, setDropTarget] = useState(null)
	const getInitialDragRD = useSelector(getInitialDrag)
	const getFinalDragRD = useSelector(getFinalDrag)
	const getTypeDragRD = useSelector(getTypeDrag)

	const getAllSnippetsRD = useSelector(getAllSnippets)

	const [modify, setModify] = useState(false)
	const [inputValueFolderName, setInputValueFolderName] = useState("")
	const [modifyFolder, setModifyFolder] = useState(null)

	const inputRef = useRef(null)

	useEffect(() => {
		if (modify && inputRef.current) {
			inputRef.current.focus()
		}
	}, [modify])

	const toggleItem = (itemId) => {
		console.log("toggleItem1", itemId)
		if (expandedItems.includes(itemId)) {
			setExpandedItems(expandedItems.filter((id) => id !== itemId))
		} else {
			setExpandedItems([...expandedItems, itemId])
		}
		console.log("toggleItem2", expandedItems)
		dispatch(fnSetFolderSnippetsSelected(itemId))
	}

	const fnSetFolder = async (item) => {
		dispatch(fnSetFolderSnippetsSelected(item.id))

		let snippetCopy = JSON.parse(JSON.stringify(getAllFolderSnippetsRD))

		let previousFolderSelected = snippetCopy.filter(
			(v) => v.isSelected === true
		)
		let newFolderSelected = snippetCopy.filter((v) => v.id === item.id)

		//deseleccionamos los folderes
		if (previousFolderSelected.length > 0) {
			for (let i = 0; i < previousFolderSelected.length; i++) {
				previousFolderSelected[i].isSelected = false
				await updateDataFolder(previousFolderSelected[0])
			}
		}

		newFolderSelected[0].isSelected = true

		await updateDataFolder(newFolderSelected[0])

		// deseleccionamos todo
		let changeSelected = getAllSnippetsRD.filter((v) => v.isSelected === true)
		if (changeSelected.length > 0) {
			for (let i = 0; i < previousFolderSelected.length; i++) {
				let setSnippet = { ...changeSelected[i], isSelected: false }
				dispatch(fnSetSnippetsSelected(setSnippet))
				await updateDataFile(setSnippet)
			}
		}

		//seleccionar el primer snippet
		if (item.id != "9e60ab5b-c17a-4758-a6fd-955e00d4b2cf") {
			let setSnippetList = getAllSnippetsRD.filter(
				(v) => v.folderId === item.id
			)
			if (setSnippetList.length > 0) {
				let setSnippet = { ...setSnippetList[0], isSelected: true }
				dispatch(fnSetSnippetsSelected(setSnippet))
				await updateDataFile(setSnippet)
			}
		} else {
			let setSnippet = { ...getAllSnippetsRD[0], isSelected: true }
			dispatch(fnSetSnippetsSelected(setSnippet))
			await updateDataFile(setSnippet)
		}
	}

	const handleEdit = (item) => {
		setModify(true)
		setModifyFolder(item)
	}

	const handleDelete = async (itemId) => {
		console.log("Delete item", itemId)

		let snippetCopy = JSON.parse(JSON.stringify(itemId))
		console.log("deleteSnippet", snippetCopy)

		let isFindChild = findChildIds(
			getAllFolderSnippetsRD,
			snippetCopy.id
		).filter((v) => v !== itemId.id)
		console.log("isFindChild", isFindChild, getAllFolderSnippetsRD)
		if (isFindChild.length > 0) {
			toast.error("Tiene que quitar las subcarpetas", {
				duration: TIME_LIMIT,
				position: POSITION_TOAST,
			})
		} else {
			let allSnippetsByFolder = getAllSnippetsRD.filter(
				(v) => v.folderId === itemId.id
			)

			if (allSnippetsByFolder.length > 0) {
				let snippetCopy = JSON.parse(JSON.stringify(allSnippetsByFolder))
				snippetCopy.map(async (v) => {
					v.folderId = "a1dc1f81-129b-46bc-975e-beb98265cc16"

					await updateDataFile(v)
				})
			}

			let returnAPI = await deleteDataJSON(
				"foldersSnippets.json",
				snippetCopy.id,
				snippetCopy
			)

			dispatch(fnAllFolderSnippets(returnAPI.data.data))
			if (returnAPI.data.data.length > 0) {
				dispatch(fnSetFolderSnippetsSelected(returnAPI.data.data[0]))
			}

			toast.success("Eliminado", {
				duration: TIME_LIMIT,
				position: POSITION_TOAST,
			})
		}
	}

	const handleChangeFolderName = (event) => {
		setInputValueFolderName(event.target.value)
	}

	const manejarKeyDown = async (event) => {
		console.log("manejarKeyDown", inputValueFolderName)
		if (event.key === "Enter") {
			let snippetCopy = JSON.parse(JSON.stringify(modifyFolder))
			snippetCopy.name = inputValueFolderName

			console.log("snippetCopy", snippetCopy)

			await updateDataFolder(snippetCopy)

			setModify(false)
			setModifyFolder(null)
		} else if (event.key === "Escape") {
			setModify(false)
			setModifyFolder(null)
		}
	}

	const handleDragStart = (item) => {
		// setDraggedItem(item)
		dispatch(fnSetInitialDrag(item))
		dispatch(fnSetType("folder"))
	}

	const handleDragOver = (event, item) => {
		event.preventDefault()
		// setDropTarget(item)
		console.log("handleDragOver", item)
		dispatch(fnSetFinalDrag(item))
	}

	const handleDrop = async (event) => {
		event.preventDefault()
		console.log("handleDrop inicial", getInitialDragRD)
		console.log("handleDrop final", getFinalDragRD)
		console.log("handleDrop type", getTypeDragRD)

		let folderInicial = { ...getInitialDragRD }

		if (getTypeDragRD == "folder") {
			folderInicial.parentId =
				getFinalDragRD.name !== "Sin Asignación" ? getFinalDragRD.id : null
			await updateDataFolder(folderInicial)
		}

		if (getTypeDragRD == "file") {
			folderInicial.folderId =
				getFinalDragRD.name !== "Sin Asignación"
					? getFinalDragRD.id
					: "a1dc1f81-129b-46bc-975e-beb98265cc16"
			await updateDataFile(folderInicial)
		}

		// setDraggedItem(null)
		// setDropTarget(null)
		dispatch(fnSetFinalDrag(""))
		dispatch(fnSetInitialDrag(""))
		dispatch(fnSetType(""))
	}

	const updateDataFolder = async (data) => {
		let returnAPI = await updateDataJSON("foldersSnippets.json", data.id, data)

		console.log("updateDataFolder", data, returnAPI)

		dispatch(fnAllFolderSnippets(returnAPI.data.data))
		// dispatch(fnSetFolderSnippetsSelected(data))

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const updateDataFile = async (data) => {
		let returnAPI = await updateDataJSON("snippets.json", data.id, data)

		console.log("updateDataFile", data, returnAPI)

		dispatch(fnAllSnippets(returnAPI.data.data))
		// dispatch(fnSetFolderSnippetsSelected(data))

		toast.success("Actualizado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	const renderButtons = (item) => {
		// console.log("renderButtons", item)
		if (hoveredItem === item.id && item.modify) {
			return (
				<div style={{ width: "40px" }}>
					<FaEdit
						style={{ cursor: "pointer", marginRight: 5 }}
						onClick={() => handleEdit(item)}
					/>
					<FaTrash
						style={{ cursor: "pointer" }}
						onClick={() => handleDelete(item)}
					/>
				</div>
			)
		}
		return null
	}

	const renderTree = (items, parentId = null, depth = 0) => {
		let marginLeftValue
		if (depth < 2) {
			marginLeftValue = `${depth * 10}px`
		} else {
			marginLeftValue = `${5 + (depth - 1) * 10}px`
		}
		return items
			.filter((item) => item.parentId === parentId)
			.map((item) => (
				<div key={item.id} style={{ marginLeft: marginLeftValue }}>
					<div
						style={{
							display: "flex",
							alignItems: "center",
							background: item.isSelected ? "lavender" : null,
							padding: "2px 5px",
							borderRadius: "5px",
						}}
						onMouseEnter={() => setHoveredItem(item.id)}
						onMouseLeave={() => setHoveredItem(null)}
						onDragOver={(event) => handleDragOver(event, item)}
						onDrop={(event) => handleDrop(event)}
					>
						<div style={{ marginLeft: marginLeftValue }}>
							{tieneSubfolder(getAllFolderSnippetsRD, item.id) ? (
								<MdOutlineKeyboardArrowDown
									onClick={() => toggleItem(item.id)}
									style={{ cursor: "pointer" }}
								/>
							) : null}
						</div>
						<FaFolder style={{ marginRight: 5 }} />
						{modify && item.id === modifyFolder.id ? (
							<input
								type="text"
								placeholder=""
								onChange={handleChangeFolderName}
								onKeyDown={manejarKeyDown}
								ref={inputRef}
								style={{ cursor: "pointer" }}
							/>
						) : (
							<span
								onClick={() => fnSetFolder(item)}
								style={{
									cursor: "pointer",
									fontWeight: item.isSelected ? "bold" : null,
									width: "150px",
								}}
							>
								{item.name}
							</span>
						)}
						<div style={{ marginLeft: "auto", width: "auto", display: "flex" }}>
							{renderButtons(item)}
							{item.modify ? (
								<div onDragStart={() => handleDragStart(item)} draggable>
									<MdDragIndicator
										style={{ cursor: "pointer", marginLeft: 10 }}
									/>
								</div>
							) : null}
						</div>
					</div>
					{expandedItems.includes(item.id) && (
						<>{renderTree(items, item.id, depth + 1)}</>
					)}
				</div>
			))
	}

	return <div>{renderTree(data)}</div>
}

export default TreeView
