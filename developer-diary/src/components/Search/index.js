import toast from "react-hot-toast"
import { FaDeleteLeft } from "react-icons/fa6"
import { BsFillFileEarmarkPlusFill } from "react-icons/bs"

import React, { useState, useEffect } from "react"

import { useDispatch, useSelector } from "react-redux"

import { v4 as uuidv4 } from "uuid"

import {
	getAllSnippets,
	fnSetSnippetsSeach,
	fnSetSnippetsValueSeach,
	getValueSearchSnippet,
	fnAllSnippets,
} from "../../store/reducers/snippetsReducer"

import { getSelectedFolderSnippets } from "../../store/reducers/folderSnippetsReducer"

import { templateSnippets, setDataJSON, toastConfig } from "../../helper"

const TIME_LIMIT = toastConfig.time
const POSITION_TOAST = toastConfig.position

const MiComponente = () => {
	const terminoBusqueda = useSelector(getValueSearchSnippet)
	const getSelectedFolderSnippetsRD = useSelector(getSelectedFolderSnippets)

	const jsonData = useSelector(getAllSnippets)

	const dispatch = useDispatch()

	const handleChange = (event) => {
		let valueSearch = event.target.value
		dispatch(fnSetSnippetsValueSeach(valueSearch))
		if (valueSearch) {
			buscarPorCampos(valueSearch)
		} else {
			dispatch(fnSetSnippetsSeach([]))
		}
	}

	// const buscarPorCampos = (termino) => {
	// 	const resultados = []

	// 	jsonData.forEach((item) => {
	// 		// Buscar en los campos "label" y "value" dentro de "content"
	// 		item.content.forEach((contentItem) => {
	// 			if (
	// 				contentItem.label.toLowerCase().includes(termino.toLowerCase()) ||
	// 				contentItem.value.toLowerCase().includes(termino.toLowerCase())
	// 			) {
	// 				resultados.push(item)
	// 			}
	// 		})

	// 		// Buscar en la descripción
	// 		if (item.description.toLowerCase().includes(termino.toLowerCase())) {
	// 			resultados.push(item)
	// 		}

	// 		// Buscar por el nombre
	// 		if (item.name.toLowerCase().includes(termino.toLowerCase())) {
	// 			resultados.push(item)
	// 		}
	// 	})

	// 	// Eliminar duplicados si es necesario
	// 	const resultadosUnicos = [...new Set(resultados)]

	// 	dispatch(fnSetSnippetsSeach(resultadosUnicos))
	// 	console.log("resultadosUnicos", resultadosUnicos)
	// }

	const buscarPorCampos = (termino) => {
		const resultados = []

		// Normaliza el término de búsqueda para manejar tildes y caracteres especiales
		const terminoNormalizado = termino
			.toLowerCase()
			.normalize("NFD")
			.replace(/[\u0300-\u036f]/g, "")

		jsonData.forEach((item) => {
			// Normaliza los campos "label", "value" y "description" para manejar tildes y caracteres especiales
			const labelNormalizado = item.content.map((contentItem) =>
				contentItem.label
					.toLowerCase()
					.normalize("NFD")
					.replace(/[\u0300-\u036f]/g, "")
			)
			const valueNormalizado = item.content.map((contentItem) =>
				contentItem.value
					.toLowerCase()
					.normalize("NFD")
					.replace(/[\u0300-\u036f]/g, "")
			)
			const descriptionNormalizado = item.description
				.toLowerCase()
				.normalize("NFD")
				.replace(/[\u0300-\u036f]/g, "")

			// Buscar en los campos "label" y "value" dentro de "content"
			if (
				labelNormalizado.some((label) => label.includes(terminoNormalizado)) ||
				valueNormalizado.some((value) => value.includes(terminoNormalizado))
			) {
				resultados.push(item)
			}

			// Buscar en la descripción
			if (descriptionNormalizado.includes(terminoNormalizado)) {
				resultados.push(item)
			}

			// Buscar por el nombre
			const nombreNormalizado = item.name
				.toLowerCase()
				.normalize("NFD")
				.replace(/[\u0300-\u036f]/g, "")
			if (nombreNormalizado.includes(terminoNormalizado)) {
				resultados.push(item)
			}
		})

		// Eliminar duplicados si es necesario
		const resultadosUnicos = [...new Set(resultados)]

		dispatch(fnSetSnippetsSeach(resultadosUnicos))
		console.log("resultadosUnicos", resultadosUnicos)
	}

	const clearSearch = () => {
		dispatch(fnSetSnippetsValueSeach(""))
		dispatch(fnSetSnippetsSeach([]))
	}

	const createSnippet = async () => {
		console.log("createSnippet")

		let id = uuidv4()

		let data = {
			...templateSnippets,
			id: id,
			name: "temp-file",
			folderId: getSelectedFolderSnippetsRD
				? getSelectedFolderSnippetsRD
				: "a1dc1f81-129b-46bc-975e-beb98265cc16",
			content: [
				{
					label: "Fragment",
					value: "",
					language: "plaintext",
				},
			],
		}

		let returnAPI = await setDataJSON("snippets.json", data)

		dispatch(fnAllSnippets(returnAPI.data.data))

		toast.success("Creado", {
			duration: TIME_LIMIT,
			position: POSITION_TOAST,
		})
	}

	return (
		<div
			style={{
				marginBottom: "20px",
				display: "flex",
				justifyContent: "space-between",
				alignItems: "center",
			}}
		>
			<div
				style={{
					display: "flex",
					justifyCcontent: "center",
					alignItems: "center",
				}}
			>
				<label style={{ marginRight: "5px" }}>Buscar:</label>
				<input
					type="text"
					value={terminoBusqueda}
					onChange={handleChange}
					placeholder="Buscar..."
					style={{
						marginRight: "10px",
						borderRadius: "5px",
					}}
				/>
				<FaDeleteLeft
					onClick={() => clearSearch()}
					style={{ cursor: "pointer", marginRight: "5px" }}
				/>
			</div>

			<div>
				<BsFillFileEarmarkPlusFill
					onClick={createSnippet}
					style={{ marginRight: "10px", cursor: "pointer" }}
				/>
			</div>
		</div>
	)
}

export default MiComponente
