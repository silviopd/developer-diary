import "./fontFamily.css"

import { Toaster } from "react-hot-toast"

import React, { useState, useEffect } from "react"

import { useDispatch } from "react-redux"
import {
	fnAllFolderNotes,
	fnSetFolderNotesSelected,
} from "../../store/reducers/folderNotesReducer"

import {
	fnAllFolderNotesDev,
	fnSetFolderNotesDevSelected,
} from "../../store/reducers/folderNotesDevReducer"

import {
	fnAllFolderSnippets,
	fnSetFolderSnippetsSelected,
} from "../../store/reducers/folderSnippetsReducer"

import { fnAllStatus } from "../../store/reducers/statusReducer"
import { fnAllTags } from "../../store/reducers/tagsReducer"
import { fnAllTypes } from "../../store/reducers/typesReducer"
import { fnAllWorks } from "../../store/reducers/worksReducer"
import {
	fnAllSnippets,
	fnSetSnippetsSelected,
} from "../../store/reducers/snippetsReducer"
import { fnAllNotes } from "../../store/reducers/notesReducer"

import { getDataJSON, updateDataJSON } from "../../helper"

import Snippets from "../Snippet"

import Loader from "../Loader/index"

import { Tab, Tabs, TabList, TabPanel } from "react-tabs"
import "react-tabs/style/react-tabs.css"

export default function Lista() {
	const [dataLoaded, setDataLoaded] = useState(false)
	const dispatch = useDispatch()

	useEffect(() => {
		const fetchData = async () => {
			try {
				const getAllFoldersNotes = await getDataJSON("foldersNotes.json")
				dispatch(fnAllFolderNotes(getAllFoldersNotes))

				let setFolderNotesDefault =
					getAllFoldersNotes.length > 0 ? getAllFoldersNotes[0].id : ""
				dispatch(fnSetFolderNotesSelected(setFolderNotesDefault))

				const getAllFoldersNotesDev = await getDataJSON("foldersNotesDev.json")
				dispatch(fnAllFolderNotesDev(getAllFoldersNotesDev))

				let setFolderNotesDevDefault =
					getAllFoldersNotesDev.length > 0 ? getAllFoldersNotesDev[0].id : ""
				dispatch(fnSetFolderNotesDevSelected(setFolderNotesDevDefault))

				const getAllFoldersSnippetsDev = await getDataJSON(
					"foldersSnippets.json"
				)
				// dispatch(fnAllFolderSnippets(getAllFoldersSnippetsDev))

				let setFolderSnippetsDefault =
					getAllFoldersSnippetsDev.length > 0
						? getAllFoldersSnippetsDev[0].id
						: ""
				dispatch(fnSetFolderSnippetsSelected(setFolderSnippetsDefault))

				if (getAllFoldersSnippetsDev.length > 0) {
					//sacamos todos los isselected y reseteamos
					let setAllFolderSnippets = JSON.parse(
						JSON.stringify(getAllFoldersSnippetsDev)
					)

					let resetFolderSnippets = setAllFolderSnippets.filter(
						(v) => v.isSelected === true
					)

					setAllFolderSnippets.forEach((v, i) => {
						v.isSelected = false
						if (i == 0) v.isSelected = true
					})
					console.log("setAllFolderSnippets", setAllFolderSnippets)
					dispatch(fnAllFolderSnippets(setAllFolderSnippets))

					await updateDataFolder({
						...setAllFolderSnippets[0],
						isSelected: true,
					})

					if (resetFolderSnippets.length > 0) {
						for (let i = 0; i < resetFolderSnippets.length; i++) {
							let reset = { ...resetFolderSnippets[i], isSelected: false }
							await updateDataFolder(reset)
						}
					}
				}

				const getAllStatus = await getDataJSON("status.json")
				const getAllTags = await getDataJSON("tags.json")
				const getAllTypes = await getDataJSON("types.json")
				const getAllWorks = await getDataJSON("works.json")
				const getAllNotes = await getDataJSON("notesDev.json")
				const getAllSnippets = await getDataJSON("snippets.json")

				dispatch(fnAllStatus(getAllStatus))
				dispatch(fnAllTags(getAllTags))
				dispatch(fnAllTypes(getAllTypes))
				dispatch(fnAllWorks(getAllWorks))
				// dispatch(fnAllSnippets(getAllSnippets))
				dispatch(fnAllNotes(getAllNotes))

				if (setFolderSnippetsDefault != "") {
					let copyGetAllSnippets = JSON.parse(JSON.stringify(getAllSnippets))
					let setSnippetsSelected = null
					let flag = false

					copyGetAllSnippets.forEach((v) => {
						v.isSelected = false
						if (v.folderId === setFolderSnippetsDefault && !flag) {
							v.isSelected = true
							setSnippetsSelected = { ...v, isSelected: true }
							flag = true
						}
					})

					console.log("setSnippetsSelected", setSnippetsSelected)
					console.log("copyGetAllSnippets", copyGetAllSnippets)

					if (setSnippetsSelected != null) {
						dispatch(fnSetSnippetsSelected(setSnippetsSelected))
						await updateDataFile(setSnippetsSelected)
					}

					dispatch(fnAllSnippets(copyGetAllSnippets))
				} else {
					dispatch(fnAllSnippets(getAllSnippets))
				}

				setDataLoaded(true)
			} catch (error) {
				console.error("Error en la obtención de datos:", error)
			}
		}

		if (!dataLoaded) {
			fetchData()
		}
	}, [dataLoaded])

	const updateDataFile = async (data) => {
		let returnAPI = await updateDataJSON("snippets.json", data.id, data)

		dispatch(fnAllSnippets(returnAPI.data.data))
	}

	const updateDataFolder = async (data) => {
		let returnAPI = await updateDataJSON("foldersSnippets.json", data.id, data)

		dispatch(fnAllFolderSnippets(returnAPI.data.data))
	}

	return dataLoaded ? (
		<>
			<Tabs>
				<TabList>
					<Tab key="1">Snippets</Tab>
					<Tab key="2">Notas</Tab>
				</TabList>
				<TabPanel key="1">
					<Toaster />
					<Snippets />
				</TabPanel>
				<TabPanel key="2">
					<h2>ESTO ES NOTAS</h2>
				</TabPanel>
			</Tabs>
		</>
	) : (
		<Loader />
	)
}
