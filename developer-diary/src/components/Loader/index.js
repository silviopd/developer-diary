import Loader from "rsuite/Loader"
import "rsuite/Loader/styles/index.css"

export default function index() {
	return (
		<div>
			<Loader center size="lg" content="" />
		</div>
	)
}
