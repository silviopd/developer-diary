import { configureStore } from "@reduxjs/toolkit"
import folderNotesReducer from "./reducers/folderNotesReducer"
import folderNotesDevReducer from "./reducers/folderNotesDevReducer"
import folderSnippetsReducer from "./reducers/folderSnippetsReducer"
import statusReducer from "./reducers/statusReducer"
import tagsReducer from "./reducers/tagsReducer"
import typesReducer from "./reducers/typesReducer"
import worksReducer from "./reducers/worksReducer"
import snippetsReducer from "./reducers/snippetsReducer"
import notesReducer from "./reducers/notesReducer"
import dragReducer from "./reducers/dragReducer"

export const store = configureStore({
	reducer: {
		folderNotes: folderNotesReducer,
		folderNotesDev: folderNotesDevReducer,
		folderSnippets: folderSnippetsReducer,
		status: statusReducer,
		tags: tagsReducer,
		types: typesReducer,
		works: worksReducer,
		snippets: snippetsReducer,
		notes: notesReducer,
		drag: dragReducer,
	},
})

export const RootState = store.getState
export const AppDispatch = store.dispatch
