import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	setFolderNotesDev: "",
	allFolderNotesDev: [],
}

export const folderNotesDevSlice = createSlice({
	name: "folderNotesDev",
	initialState,
	reducers: {
		fnSetFolderNotesDevSelected: (state, action) => {
			state.setFolderNotesDev = action.payload
		},
		fnAllFolderNotesDev: (state, action) => {
			state.allFolderNotesDev = [...action.payload]
		},
	},
})

export const { fnSetFolderNotesDevSelected, fnAllFolderNotesDev } =
	folderNotesDevSlice.actions
export const getAllFolderNotesDev = (state) =>
	state.folderNotesDev.allFolderNotesDev
export const getSelectedFolderNotesDev = (state) =>
	state.folderNotesDev.setFolderNotesDev

export default folderNotesDevSlice.reducer
