import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	setFolderSnippets: "",
	allFolderSnippets: [],
}

export const folderSnippetsSlice = createSlice({
	name: "folderSnippets",
	initialState,
	reducers: {
		fnSetFolderSnippetsSelected: (state, action) => {
			state.setFolderSnippets = action.payload
		},
		fnAllFolderSnippets: (state, action) => {
			state.allFolderSnippets = [...action.payload]
		},
	},
})

export const { fnSetFolderSnippetsSelected, fnAllFolderSnippets } =
	folderSnippetsSlice.actions
export const getAllFolderSnippets = (state) =>
	state.folderSnippets.allFolderSnippets
export const getSelectedFolderSnippets = (state) =>
	state.folderSnippets.setFolderSnippets

export default folderSnippetsSlice.reducer
