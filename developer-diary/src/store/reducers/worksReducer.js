import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	allWorks: [],
}

export const worksSlice = createSlice({
	name: "works",
	initialState,
	reducers: {
		fnAllWorks: (state, action) => {
			state.allWorks = [...action.payload]
		},
	},
})

export const { fnAllWorks } = worksSlice.actions
export const getAllWorks = (state) => state.works.allWorks

export default worksSlice.reducer
