import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	initialDrag: "",
	finalDrag: "",
	type: "",
}

export const dragSlice = createSlice({
	name: "drag",
	initialState,
	reducers: {
		fnSetInitialDrag: (state, action) => {
			state.initialDrag = action.payload
		},
		fnSetFinalDrag: (state, action) => {
			state.finalDrag = action.payload
		},
		fnSetType: (state, action) => {
			state.type = action.payload
		},
	},
})

export const { fnSetInitialDrag, fnSetFinalDrag, fnSetType } = dragSlice.actions

export const getInitialDrag = (state) => state.drag.initialDrag
export const getFinalDrag = (state) => state.drag.finalDrag
export const getTypeDrag = (state) => state.drag.type

export default dragSlice.reducer
