import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	setNotes: [],
	allNotes: [],
}

export const notesSlice = createSlice({
	name: "notes",
	initialState,
	reducers: {
		fnSetNotesSelected: (state, action) => {
			state.setNotes = action.payload
		},
		fnAllNotes: (state, action) => {
			state.allNotes = [...action.payload]
		},
	},
})

export const { fnSetNotesSelected, fnAllNotes } = notesSlice.actions
export const getAllNotes = (state) => state.notes.allNotes
export const getSetNotes = (state) => state.notes.setNotes

export default notesSlice.reducer
