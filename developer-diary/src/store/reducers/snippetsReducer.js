import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	setSnippets: [],
	allSnippets: [],
	parentToChild: [],
	searchSnippets: [],
	valueSearch: "",
}

export const snippetsSlice = createSlice({
	name: "snippets",
	initialState,
	reducers: {
		fnSetSnippetsSelected: (state, action) => {
			state.setSnippets = action.payload
		},
		fnAllSnippets: (state, action) => {
			state.allSnippets = [...action.payload]
		},
		fnParentToChild: (state, action) => {
			state.parentToChild = [...action.payload]
		},
		fnSetSnippetsSeach: (state, action) => {
			state.searchSnippets = action.payload
		},
		fnSetSnippetsValueSeach: (state, action) => {
			state.valueSearch = action.payload
		},
	},
})

export const {
	fnSetSnippetsSelected,
	fnAllSnippets,
	fnParentToChild,
	fnSetSnippetsSeach,
	fnSetSnippetsValueSeach,
} = snippetsSlice.actions
export const getAllSnippets = (state) => state.snippets.allSnippets
export const getSelectedSnippet = (state) => state.snippets.setSnippets
export const getSearchSnippet = (state) => state.snippets.searchSnippets
export const getValueSearchSnippet = (state) => state.snippets.valueSearch

export default snippetsSlice.reducer
