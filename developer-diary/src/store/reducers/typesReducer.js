import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	allTypes: [],
}

export const typesSlice = createSlice({
	name: "types",
	initialState,
	reducers: {
		fnAllTypes: (state, action) => {
			state.allTypes = [...action.payload]
		},
	},
})

export const { fnAllTypes } = typesSlice.actions
export const getAllTypes = (state) => state.types.allTypes

export default typesSlice.reducer
