import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	allTags: [],
}

export const tagsSlice = createSlice({
	name: "tags",
	initialState,
	reducers: {
		fnAllTags: (state, action) => {
			state.allTags = [...action.payload]
		},
	},
})

export const { fnAllTags } = tagsSlice.actions
export const getAllTags = (state) => state.tags.allTags

export default tagsSlice.reducer
