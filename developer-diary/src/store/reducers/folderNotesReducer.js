import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	setFolderNotes: "",
	allFolderNotes: [],
}

export const folderNotesSlice = createSlice({
	name: "folderNotes",
	initialState,
	reducers: {
		fnSetFolderNotesSelected: (state, action) => {
			state.setFolderNotes = action.payload
		},
		fnAllFolderNotes: (state, action) => {
			state.allFolderNotes = [...action.payload]
		},
	},
})

export const { fnSetFolderNotesSelected, fnAllFolderNotes } =
	folderNotesSlice.actions
export const getAllFolderNotes = (state) => state.folderNotes.allFolderNotes
export const getSelectedFolderNotes = (state) =>
	state.folderNotes.setFolderNotes

export default folderNotesSlice.reducer
