import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	allStatus: [],
}

export const statusSlice = createSlice({
	name: "status",
	initialState,
	reducers: {
		fnAllStatus: (state, action) => {
			state.allStatus = [...action.payload]
		},
	},
})

export const { fnAllStatus } = statusSlice.actions
export const getAllStatus = (state) => state.status.allStatus

export default statusSlice.reducer
